# Dinky

## ROS

### Prerequisites

- Raspberry Pi Zero 2 with ROS set up as per [raspberry_pi_setup.md](raspberry_pi_setup.md).

  Packages should be installed in two ROS workspaces with everything running from `/home/pi`:

  - `ros2_galactic`: base ROS 2 Galactic packages
  - `ros2_v4l2_camera`: camera stuff
  
- Software from here copied to the Pi (in `/home/pi`):

  - `ros/src/see_apples/` to `dinky/src/see_apples/` 


### Build

    . ros2_galactic/install/local_setup.bash 
    . ros2_v4l2_camera/install/local_setup.bash 
    export MAKEFLAGS=-j1

    cd dinky
    colcon build --symlink-install --executor sequential

### Run

To start everything separately:

`v4l2_camera`:

    . ros2_galactic/install/local_setup.bash
    . ros2_v4l2_camera/install/local_setup.bash
    ros2 run v4l2_camera v4l2_camera_node --ros-args -p image_size:=[320,240] -p output_encoding:=yuv422 -p pixel_format:=YUYV

`see_apples`:

    . ros2_galactic/install/local_setup.bash
    . ros2_v4l2_camera/install/local_setup.bash
    . dinky/install/local_setup.bash
    ros2 run see_apples see_apples
