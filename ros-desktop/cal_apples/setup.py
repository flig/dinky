from setuptools import setup

package_name = 'cal_apples'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='pdenes',
    maintainer_email='pdenes@codingafter9.com',
    description='TODO: Package description',
    license_file='COPYING',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'cal_apples = cal_apples.cal_apples:main',
            'fake_image = cal_apples.fake_image:main',
        ],
    },
)
