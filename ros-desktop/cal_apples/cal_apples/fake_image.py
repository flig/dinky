# Copyright 2022, Pal Denes. Licensed under the AGPL v3, see COPYING.
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge
import cv2
from os import path


class FakeImage(Node):
    """
    Publishes a fake camera image and focal length.

    Publishes test.png to /image_raw and a hard-coded fake focal
    length in an otherwise empty CameraInfo to /camera_info.
    """

    def __init__(self):
        super().__init__('fake_image')
        self.camera_info_publisher = self.create_publisher(CameraInfo, 'camera_info', 10)
        self.image_publisher = self.create_publisher(Image, 'image_raw', 10)
        timer_period = 1  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.cv_bridge = CvBridge()
        self.get_logger().info('Started!')

    def timer_callback(self):
        filename = path.join(path.dirname(__file__), 'test.png')
        image = cv2.cvtColor(cv2.imread(filename), cv2.COLOR_BGR2RGB)
        self.image_publisher.publish(self.cv_bridge.cv2_to_imgmsg(image, encoding='rgb8'))

        info_msg = CameraInfo()
        info_msg.k[0] = 168  # fake focal length
        self.camera_info_publisher.publish(info_msg)

        self.get_logger().info('Published')


def main(args=None):
    rclpy.init(args=args)
    rclpy.spin(FakeImage())
    rclpy.shutdown()


if __name__ == '__main__':
    main()
