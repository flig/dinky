# Copyright 2022, Pal Denes. Licensed under the AGPL v3, see COPYING.
from struct import Struct
from math import isnan


def read_points(msg):
    """
    Read points from PointCloud2.

    Based on the old ROS implementation:
    http://docs.ros.org/en/api/sensor_msgs/html/point__cloud2_8py_source.html#l00060
    """
    unpack_from = Struct("<fff").unpack_from
    for v in range(msg.height):
        offset = msg.row_step * v
        for u in range(msg.width):
            p = unpack_from(msg.data, offset)
            has_nan = False
            for pv in p:
                if isnan(pv):
                    has_nan = True
                    break
            if not has_nan:
                yield p
            offset += msg.point_step
