# Copyright 2022, Pal Denes. Licensed under the AGPL v3, see COPYING.
import rclpy
from rclpy.node import Node

from sensor_msgs.msg import Image
from sensor_msgs.msg import PointCloud2
from cv_bridge import CvBridge
import cv2

from cal_apples.read_points import read_points


class CalApples(Node):

    def __init__(self):
        super().__init__('cal_apples')

        self.create_subscription(
            Image,
            'image_raw',
            self.image_listener_callback,
            10)

        self.create_subscription(
            Image,
            'apples/image',
            self.apples_image_listener_callback,
            10)

        self.create_subscription(
            PointCloud2,
            'apples',
            self.cloud_listener_callback,
            10)

        self.cv_bridge = CvBridge()
        self.get_logger().info('Started')

    def cloud_listener_callback(self, msg):
        self.get_logger().info('point cloud received:\n' +
                               '\n'.join([f'   {p[0]: 6.3f}, {p[1]: 6.3f}, {p[2]: 6.3f}'
                                          for p in read_points(msg)]))

    def image_listener_callback(self, msg):
        self.get_logger().info(f'image received: {msg.encoding}')

        # TODO when we don't need all the channels, use extractChannel instead of split?

        if msg.encoding == 'yuv422':
            img = self.cv_bridge.imgmsg_to_cv2(msg, 'passthrough')
            # u+v interlaced
            y, uv = cv2.split(img)
            uv = uv.reshape((uv.shape[0], int(uv.shape[1]/2), 2))
            u, v = cv2.split(uv)
            u = cv2.resize(u, (img.shape[1], img.shape[0]))
            v = cv2.resize(v, (img.shape[1], img.shape[0]))
            # just so that we can show it as normal bgr:
            img = cv2.cvtColor(img, cv2.COLOR_YUV2BGR_YUYV)
            cv2.imshow('image_raw', img)
            cv2.imshow('v', v)
            cv2.waitKey(1)
        elif msg.encoding == 'rgb8':
            img = self.cv_bridge.imgmsg_to_cv2(msg, 'bgr8')
            img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
            y, u, v = cv2.split(img_yuv)
            cv2.imshow('image_raw', img)
            cv2.imshow('v', v)
            cv2.waitKey(1)
        else:
            self.get_logger().error(f'unsupported encoding: {msg.encoding}')
            return

    def apples_image_listener_callback(self, msg):
        self.get_logger().info(f'apples/image received: {msg.encoding}')
        img = self.cv_bridge.imgmsg_to_cv2(msg, 'passthrough')
        cv2.imshow('apples/image', img)
        cv2.waitKey(1)


def main(args=None):
    rclpy.init(args=args)
    cal_apples = CalApples()
    rclpy.spin(cal_apples)
    cal_apples.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
