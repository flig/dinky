include <./common.scad>
include <./xl-330.scad>
use <./xl-330-bottom-bracket.scad>

$fn = 50;

_lower_len = 23; // first try: 26;
xl_330_joint_bracket_servo_offs = [xl_330_w/2, 0, xl_330_horn_z_pos];

function min_xl_330_link_len() = sqrt(pow(xl_330_w/2, 2) + pow(xl_330_h - xl_330_horn_z_pos, 2)) + 1; // diagonal, to clear the top corner

function offset_for_bend_angle(a) = sin(a) * _lower_len;


module _joint_link_hole(z) {
  translate([0, 10, z]) rotate([90,0,0]) cylinder(d = m2_screw_hole_d, h = xl_330_link_d + 20);
}

module _joint_link_holes(l) {
  if (l >= 40) {
    _joint_link_hole(l - 16);
    _joint_link_hole(l/2 + 2);
  }
}

// bit to join a link to the connector betwen links
module _link_to_connector(long = false, bottom_part_only = false, thick_bottom = false) {
  extra = (xl_330_bottom_bracket_d() - xl_330_bottom_bracket_inner_d()) / 2;
  yoffs = xl_330_bracket_thickness + 0.1 + extra;

  dd = xl_330_link_d + 1;

  module bottom_part() {
    h = thick_bottom ? 3 : 1;
    translate([-xl_330_bottom_bracket_w()/2, -yoffs, -h]) cube([xl_330_bottom_bracket_w(), yoffs + dd, h]);
  }
  
  translate([0, -dd, 0]) {
  
    if (bottom_part_only) {
      bottom_part();
    } else {
      difference() {
        union() {
          hull() {
            bottom_part();
            translate([-xl_330_link_w/2, dd - xl_330_link_d, long ? -8 : -5]) cube([xl_330_link_w, xl_330_link_d, 0.1]);
          }
          translate([-xl_330_bottom_bracket_w()/2, -yoffs, 0]) {
            cube([xl_330_bottom_bracket_w(), dd, xl_330_bottom_bracket_h()]);
          }
        }
        translate([xl_330_w/2 - xl_330_mounting_holes_dist, 0, xl_330_mounting_holes_dist + xl_330_bracket_thickness]) rotate([90,0,0]) cylinder(d=m2_screw_hole_d, h=10);
        translate([-xl_330_w/2 + xl_330_mounting_holes_dist, 0, xl_330_mounting_holes_dist + xl_330_bracket_thickness]) rotate([90,0,0]) cylinder(d=m2_screw_hole_d, h=10);
      }
    }

  }
}

module xl_330_joint_horn_link(l = min_xl_330_link_len(), bend_angle = 0, thin_top = false, rounded_end = false) {
  offs2 = offset_for_bend_angle(bend_angle);
  long = l > 40;
  l1 = cos(bend_angle) * _lower_len;
  joint_d = xl_330_link_w+1;

  translate([0, xl_330_d + xl_330_joint_bracket_y_gap, 0]) difference() {
    if (long) {
      union() {
        if (bend_angle != 0) {
          translate([offs2 - xl_330_link_w/2, 0, l1]) {
            translate([xl_330_link_w / 2, 0, 0]) rotate([-90,0,0]) cylinder(d = joint_d, h = xl_330_link_d);
          }
        }

        rotate([0, bend_angle, 0]) translate([-xl_330_link_w/2, 0, 15*0]) cube([xl_330_link_w, xl_330_link_d, _lower_len]);
        hull() {
          rotate([0, bend_angle, 0]) {
            rotate([-90,0,0]) cylinder(d = xl_330_joint_outer_dia, h = xl_330_joint_d);
          }
          rotate([0, bend_angle, 0]) translate([-xl_330_link_w/2, 0, 15]) cube([xl_330_link_w, xl_330_link_d, 0.1]);
        }
        translate([offs2-xl_330_link_w/2, 0, l1]) {
          cube([xl_330_link_w, xl_330_link_d, l-l1]);
        }
      }
    } else {
      union() {
        rotate([0, bend_angle, 0]) {
          rotate([-90,0,0]) cylinder(d = xl_330_joint_outer_dia, h = xl_330_joint_d);
          if (!thin_top) {
            translate([-xl_330_joint_outer_dia/2, 0, 0]) cube([xl_330_joint_outer_dia, xl_330_joint_d, xl_330_joint_outer_dia/2]);
          }
        }
        if (thin_top) {
          translate([-xl_330_link_w/2, 0, 0]) {
            cube([xl_330_link_w, xl_330_link_d, l]);            
          }
        } else {
          hull() {
            translate([-xl_330_joint_outer_dia/2, 0, xl_330_joint_outer_dia/2]) cube([xl_330_joint_outer_dia, xl_330_joint_d, 0.1]);
            translate([offs2, 0, l]) mirror([0,1,0]) _link_to_connector(long = false, bottom_part_only = true);
          }
        }
        if (rounded_end) {
          translate([0, xl_330_link_d, l]) rotate([90,0,0]) cylinder(d = xl_330_link_w, h = xl_330_link_d);
        }
      }
    }
    if (l >= 40) {
      rotate([0, bend_angle, 0]) _joint_link_hole(_lower_len);
    }
    rotate([-90,0,0]) translate([0, 0, -1]) cylinder(d = xl_330_horn_dia + xl_330_joint_horn_dia_extend, h = xl_330_horn_h + 1);
    translate([0, -1, 0]) rotate([-90, 0, 0]) xl_330_horn_holes(h = 10, dia = 1.4);
    translate([offs2, 0, 0]) _joint_link_holes(l);
  }
}

// "Universal" bracket: just a joint with a platform: horn-side half
module xl_330_joint_platform_horn_side(l = min_xl_330_link_len(), bend_angle = 0) {
  xl_330_joint_horn_link(l, bend_angle);
  translate([offset_for_bend_angle(bend_angle), xl_330_d + xl_330_joint_bracket_y_gap, l]) mirror([0,1,0]) _link_to_connector(long = l > 40, thick_bottom = true);
}

module xl_330_joint_opposite_link(l = min_xl_330_link_len(), bend_angle = 0, clear_cable = false, rounded_end = false, w_offset = 0, w_offset_top = 0, flatten_bottom = false) {
  pos1 = _lower_len - 4;
  pos2 = 12; // first try: 15;
  offs2 = offset_for_bend_angle(bend_angle);

  opposite_hole_reduce = 0.3;
  offs = clear_cable ? 10 : w_offset;

  module hub() {
    if (!flatten_bottom) {
      /* no need for this when printing horizontally */
      translate([0, xl_330_joint_bracket_y_gap, 0]) rotate([90,0,0]) cylinder(d = xl_330_opposite_outer_hole_dia + 1, h = 0.2);
    }
    translate([0, xl_330_opposite_outer_hole_d + xl_330_joint_bracket_y_gap, 0]) rotate([90,0,0]) cylinder(d = xl_330_opposite_outer_hole_dia - opposite_hole_reduce - (flatten_bottom ? 0.2 : 0) /* adjust for printing horizontally */, h = xl_330_opposite_outer_hole_d + xl_330_joint_bracket_y_gap);
    rotate([90,0,0]) cylinder(d = xl_330_opposite_joint_dia, h = xl_330_opposite_joint_d + offs);
    if (offs > xl_330_link_d * 2) {
      translate([0, -xl_330_opposite_joint_d - offs + xl_330_link_d * 2]) rotate([90,0,0]) cylinder(d1 = xl_330_opposite_joint_dia, d2 = xl_330_opposite_joint_dia + 2, h = xl_330_link_d * 2);
    }
  }

  translate([0, -xl_330_joint_bracket_y_gap, 0]) difference() {
    union() {
      hub();
      //top straight section
      translate([-xl_330_link_w/2, -xl_330_link_d + w_offset_top, 0]) {
        translate([offs2, 0, pos1]) cube([xl_330_link_w, xl_330_link_d, l - pos1]);
      }

      // bottom straight section
      if (pos2 < l) {
        rotate([0, bend_angle, 0]) translate([-xl_330_link_w/2, -xl_330_link_d - offs, 0]) cube([xl_330_link_w, xl_330_link_d, pos2]);
      }
        
      if (offs > 0) {
        translate([0, -xl_330_link_d, 0]) {
          hull() {
            translate([offs2-xl_330_link_w/2, w_offset_top, pos1 - xl_330_joint_bracket_y_gap-1.11]) rotate([0, bend_angle, 0]) cube([xl_330_link_w, xl_330_link_d, 1.1]);
            translate([offs2-xl_330_link_w/2, w_offset_top, pos1]) cube([xl_330_link_w, xl_330_link_d, 0.1]);
          }
          hull() {
            translate([offs2-xl_330_link_w/2, w_offset_top, pos1 - xl_330_joint_bracket_y_gap-1.11]) rotate([0, bend_angle, 0]) cube([xl_330_link_w, xl_330_link_d, 1.1]);
            rotate([0, bend_angle, 0]) translate([-xl_330_link_w/2, -offs, pos2]) cube([xl_330_link_w, xl_330_link_d, 1.1]);
          }
        }
        if (rounded_end) {
          translate([0, w_offset_top, l]) {
            difference() {
              rotate([90,0,0]) cylinder(d = xl_330_link_w, h = xl_330_link_d);
              translate([-xl_330_link_w, -xl_330_link_d - 1, -xl_330_link_w]) cube([xl_330_link_w * 2, xl_330_link_d + 2, xl_330_link_w]);
            }
          }
        }
      } else {
        if (pos1 < l) {
          translate([0, -xl_330_link_d, 0]) {
            hull() {
              translate([offs2 - xl_330_link_w/2, 0, pos1]) cube([xl_330_link_w, xl_330_link_d, 0.1]);
              rotate([0, bend_angle, 0]) translate([-xl_330_link_w/2, -offs, pos2]) cube([xl_330_link_w, xl_330_link_d, 0.1]);
            }
          }
        }
        translate([-xl_330_link_w/2, -xl_330_link_d - offs, 0]) {
          cube([xl_330_link_w, xl_330_link_d, l]);
        }
        if (rounded_end) {
          translate([0, -offs, l]) rotate([90,0,0]) cylinder(d = xl_330_link_w, h = xl_330_link_d);
        }
      }
    }
    rotate([0, bend_angle, 0]) _joint_link_hole(_lower_len);

    translate([offs2, xl_330_link_d-1, 0]) _joint_link_holes(l);
    translate([0, -xl_330_link_d-1, 0]) rotate([90,0,0]) cylinder(d = xl_330_opposite_joint_dia - 4, h = offs + 1);
    translate([0, 5, 0]) rotate([90,0,0]) cylinder(d = 2.1, h = 10 + offs);

    // flatten bottom when printing hoizontally 
    if (flatten_bottom) {
      rotate([0, bend_angle, 0]) translate([xl_330_opposite_joint_dia/2 - 0.6, -xl_330_opposite_joint_d - offs - 1, -xl_330_opposite_joint_dia/2]) cube([10, xl_330_opposite_joint_d + offs + 2, xl_330_opposite_joint_dia]);
      rotate([0, bend_angle, 0]) translate([xl_330_opposite_outer_hole_dia/2 - 0.8, 0, -xl_330_opposite_joint_dia/2]) cube([10, xl_330_opposite_outer_hole_d + xl_330_joint_bracket_y_gap + 1, xl_330_opposite_joint_dia]);
    }
  }
}

// "Universal" bracket: just a joint with a platform: opposite side half
module xl_330_joint_platform_opposite_side(l = min_xl_330_link_len(), bend_angle = 0) {
  xl_330_joint_opposite_link(l = l, bend_angle = bend_angle, clear_cable = l > 40);
  translate([offset_for_bend_angle(bend_angle), -xl_330_joint_bracket_y_gap, l]) _link_to_connector(long = l > 40);
}



function default_link_sides_connector_len() = xl_330_d + 2 * xl_330_bracket_gap;

module xl_330_link_sides_connector(l = default_link_sides_connector_len()) {
  difference() {
    cylinder(h = l, d = m2_screw_hole_d + 3);
    translate([0, 0, -1]) cylinder(h = l + 2, d = m2_screw_hole_d);
  }
}


module xl_330_joint_platform_opposite_side_lower(l) {
  ll = _lower_len;
  difference() {
    xl_330_joint_opposite_link(l = ll, bend_angle = 0, rounded_end = true, clear_cable = false, w_offset = 10, w_offset_top = xl_330_link_d + xl_330_joint_bracket_y_gap, flatten_bottom = true);
  }
}

module xl_330_joint_platform_opposite_side_upper(l, bend_angle = 0) {
  offs = offset_for_bend_angle(bend_angle);
  zoffs = cos(bend_angle) * _lower_len;

  joint_d = xl_330_link_w + 5;
  joint_d_cutout = xl_330_link_w + 0.4;

  joint_outside = false;
  
  translate([offs, 0, 0]) {
    difference() {
      union() {
        translate([-xl_330_link_w/2 , -xl_330_joint_bracket_y_gap - xl_330_link_d, zoffs]) {
          cube([xl_330_link_w, xl_330_link_d, l - zoffs]);
          translate([xl_330_link_w / 2, joint_outside ? -xl_330_link_d : 0, 0]) rotate([-90,0,0]) cylinder(d2 = joint_d, d1 = joint_d - (joint_outside ? 3 : 0), h = xl_330_link_d * 2);
        }
        translate([0, -xl_330_joint_bracket_y_gap, l]) _link_to_connector(long = l > 40, thick_bottom = true); //settings for horizontal printing
      }
    
      translate([0, -xl_330_joint_bracket_y_gap - xl_330_link_d * (joint_outside ? 2 : 1), zoffs]) {
        rotate([0, bend_angle, 0]) translate([-xl_330_link_w, -1, -8]) cube([xl_330_link_w * 2, xl_330_link_d * 2 + 2, 4]);
        translate([0, xl_330_link_d - xl_330_joint_bracket_y_gap, 0]) {
          rotate([-90, 0, 0]) cylinder(d = joint_d_cutout, h = 10);
          rotate([0, bend_angle, 0]) translate([-joint_d_cutout / 2, 0, -10]) cube([joint_d_cutout, 10, 10]);
        }
      }
      _joint_link_hole(zoffs);
      _joint_link_holes(l);
    }
  }
}


module bottom_bracket_and_joint_demo() {
  %xl_330();
  translate(xl_330_joint_bracket_servo_offs) {
    rotate([0, 45 ,0]) {
      xl_330_joint_platform_opposite_side();
      xl_330_joint_platform_horn_side();
    
      translate([-xl_330_joint_bracket_servo_offs[0], 0, min_xl_330_link_len() + xl_330_bracket_thickness]) {
        xl_330_bottom_bracket();      
      }
    }
  }
}

module bottom_bracket_90deg_and_joint_demo() {
  offs = (xl_330_w - xl_330_d)/2;
  
  %xl_330();
  translate(xl_330_joint_bracket_servo_offs) {
    rotate([0, 45 ,0]) {
      xl_330_joint_platform_opposite_side();
      xl_330_joint_platform_horn_side();
    
      translate([-xl_330_joint_bracket_servo_offs[0] + offs, -offs, min_xl_330_link_len() + xl_330_bracket_thickness]) {
        xl_330_bottom_bracket_90deg();
        %translate([xl_330_d, 0, 0]) rotate([0,0,90]) xl_330();
      }
    }
  }
}

module arm2_demo() {
  rotate([0, -45, 0]) xl_330_joint_platform_opposite_side_lower(l = 60);
  translate([0, -5, 0]) xl_330_joint_platform_opposite_side_upper(l = 60, bend_angle = -45);
  xl_330_joint_platform_horn_side(l = 60, bend_angle = -45);
}


//----------------------------------------------------------------------------------
// Demo
//----------------------------------------------------------------------------------

bottom_bracket_and_joint_demo();
//bottom_bracket_90deg_and_joint_demo();
//arm2_demo();


//----------------------------------------------------------------------------------
// Print
//----------------------------------------------------------------------------------

//arm1
//rotate([90,0,0]) xl_330_joint_platform_opposite_side(l=60);
//rotate([90,0,0]) xl_330_joint_platform_horn_side(l=60);

//arm2
//rotate([-90,0,0]) xl_330_joint_platform_horn_side(l = 60, bend_angle = -45);
//rotate([0, 90, 0]) xl_330_joint_platform_opposite_side_lower(l = 60);
//rotate([-90,90,0]) xl_330_joint_platform_opposite_side_upper(l = 60, bend_angle = -45);

//arm3
//rotate([90,0,0]) xl_330_joint_platform_opposite_side();
//rotate([-90,0,0]) xl_330_joint_platform_horn_side();

//xl_330_bottom_bracket(add_hooks = true);  // x2
//xl_330_link_sides_connector();  // x3
//xl_330_link_sides_connector(default_link_sides_connector_len() - xl_330_link_d); // x3
//xl_330_bottom_bracket_90deg();

