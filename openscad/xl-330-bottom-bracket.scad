include <./common.scad>
include <./xl-330.scad>

function xl_330_bottom_bracket_h(wall_thickness = xl_330_bracket_thickness) = xl_330_mounting_holes_dist * 2 + wall_thickness * 2;
function xl_330_bottom_bracket_inner_d(wall_thickness = xl_330_bracket_thickness, gap = xl_330_bracket_gap) = xl_330_d + (wall_thickness + gap) * 2;
function xl_330_bottom_bracket_d(wall_thickness = xl_330_bracket_thickness, gap = xl_330_bracket_gap) = xl_330_bottom_bracket_inner_d(wall_thickness, gap) + wall_thickness * 2 + m2_nut_h * 2;
function xl_330_bottom_bracket_w(wall_thickness = xl_330_bracket_thickness, gap = xl_330_bracket_gap) = xl_330_w + (wall_thickness + gap) * 2;

module _bracket(side_connectors = false, nut_boxes = true, nut_opening_angle = 180, wall_thickness = xl_330_bracket_thickness, gap = xl_330_bracket_gap, add_hooks = false) {

  nut_box_w = (xl_330_mounting_holes_dist + wall_thickness + gap) * 2;
  nut_box_d = wall_thickness + m2_nut_h;

  w = xl_330_bottom_bracket_w(wall_thickness, gap);
  h = xl_330_bottom_bracket_h(wall_thickness);
  d = xl_330_bottom_bracket_inner_d(wall_thickness, gap);

  conn_box_w = 6;
  conn_box_d = wall_thickness * 2;

  // offset to move _bracket relative to servo (at bottom)
  servo_offs = [-wall_thickness - gap, -wall_thickness - gap, -wall_thickness];

  hook_w = 5;
  hook_d = d / 2 + nut_box_d;
  module one_hook() {
    translate([0, hook_d, 0]) {
      cube([hook_w, 2, h]);
      translate([0, 0, 1]) cube([hook_w/2, xl_330_d/2 - 3, h-2]);
    }
    translate([0, hook_d + xl_330_d/2 - 3 - 3, 0]) {
      cube([hook_w/2 + 1, 3, h]);
    }
  }

  module hooks() {
    translate([-hook_w, -nut_box_d, 0]) {
      one_hook();
      translate([0, hook_d*2, 0]) mirror([0,1,0]) one_hook();
    }
    translate([w+hook_w, -nut_box_d, 0]) {
      mirror([1,0,0]) one_hook();
      mirror([1,0,0]) translate([0, hook_d*2, 0]) mirror([0,1,0]) one_hook();
    }
  }

  module captive_nuts() {
    translate([0, -nut_box_d, 0]) cube([nut_box_w, nut_box_d, h]);
    translate([w - nut_box_w, -nut_box_d, 0]) cube([nut_box_w, nut_box_d, h]);

    translate([0, d, 0]) cube([nut_box_w, nut_box_d, h]);
    translate([w - nut_box_w, d, 0]) cube([nut_box_w, nut_box_d, h]);
  }

  module captive_nuts_cutouts() {
    for (offs=[0, d + m2_nut_h]) {
      translate([0, offs, 0]) {
        translate([wall_thickness + gap + xl_330_mounting_holes_dist, -m2_nut_h, xl_330_mounting_holes_dist + wall_thickness]) {
          rotate([0, nut_opening_angle, 0]) m2_nut_cutout();
          translate([w - nut_box_w, 0, 0]) rotate([0, nut_opening_angle, 0]) m2_nut_cutout();
        }
      }
    }
  }

  translate(servo_offs) {
    if (add_hooks) {
      hooks();
    }
    difference() {
      union() {
        cube([w, d, h]);
        if (nut_boxes) {
          captive_nuts();
        }
        // connectors left-right
        if (side_connectors) {
          translate([-conn_box_w, d - conn_box_d, 0]) cube([conn_box_w, conn_box_d, h]);
          translate([w, d - conn_box_d, 0]) cube([conn_box_w, conn_box_d, h]);
        }
      }
      // cut out servo box
      translate([wall_thickness, wall_thickness, wall_thickness]) xl_330_hull(extend = gap * 2);
      // horizontal mounting holes
      translate([wall_thickness + gap, -xl_330_d/2, wall_thickness]) xl_330_mounting_holes(h = xl_330_d * 2, dia = m2_screw_hole_d);
      if (nut_boxes) {
        captive_nuts_cutouts();
      }
      // connectors left-right
      if (side_connectors) {
        translate([-conn_box_w/2, 0, h/2]) rotate([-90,0,0]) cylinder(d = m2_screw_hole_d, h = d * 2);
        translate([w + conn_box_w/2, 0, h/2]) rotate([-90,0,0]) cylinder(d = m2_screw_hole_d, h = d * 2);
      }
    }
  }
}

module xl_330_bottom_bracket(nut_opening_angle = 180, wall_thickness = xl_330_bracket_thickness, gap = xl_330_bracket_gap, add_hooks = false, nut_boxes = true) {
  difference() {
    _bracket(nut_opening_angle = nut_opening_angle, wall_thickness = wall_thickness, gap = gap, add_hooks = add_hooks, nut_boxes = nut_boxes);
    // bottom cutout
    translate([1, 2, -2]) cube([5, xl_330_d - 4, wall_thickness + 2]);
    translate([xl_330_w - 4 - 2, 2, -2]) cube([5, xl_330_d - 4, wall_thickness + 2]);
    translate([xl_330_w/2 - 2, 2, -2]) cube([4, xl_330_d - 4, wall_thickness + 2]);
  }
}

module xl_330_bottom_bracket_90deg(nut_opening_angle = 180) {
  difference() {
    union() {
      offs = (xl_330_w - xl_330_d) / 2;
      translate([-offs, offs, 0]) xl_330_bottom_bracket(nut_opening_angle = nut_opening_angle);
      translate([xl_330_d, 0, 0]) rotate([0,0,90]) xl_330_bottom_bracket(nut_opening_angle = nut_opening_angle);
    }
    // cut out servo box
    translate([xl_330_d + xl_330_bracket_gap, -xl_330_bracket_gap, 0]) rotate([0, 0, 90]) xl_330_hull(extend = xl_330_bracket_gap * 2);
  }
}

