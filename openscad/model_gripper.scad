$fn = 50;

include <xl-330.scad>
use <xl-330-brackets.scad>
use <xl-330-bottom-bracket.scad>
use <gripper.scad>

echo("gripper_origin:", -gripper_origin_to_bottom_servo_offs() / 1000 + [0, 0.015, 0]); // adjust Y a bit...
echo("camera_origin:", (-gripper_origin_to_bottom_servo_offs() + camera_holder_offset()) / 1000);
  
translate(-gripper_origin_to_bottom_servo_offs()) {
  gripper_demo(servo = true);
  gripper_demo_wide_open();
  translate(camera_holder_offset()) rotate([-90, 0, 0]) cylinder(h = 2, d = 5);
}



