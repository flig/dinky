// A very crude approximation of Flig's body
use <xl-330-base.scad>

$fn=50;

d = 230;
w = 110;

h_stand = 90;
h_sit = 70;
base_height = xl_330_base_bottom_total_height() + xl_330_base_cover_total_height();

module flig_body() {
  translate([-w/2, -d/2, -h_sit - base_height]) cube([w, d, h_sit]);

  module basket() {
    yoffs = 95;
    hull() {
      translate([-60/2, -68 -yoffs, -base_height]) cube([60, 68, 0.1]);
      translate([-161/2, -68 - (118-68) -yoffs, 50-base_height]) cube([161, 118, 0.1]);
    }
  }
  basket();
}

flig_body();
