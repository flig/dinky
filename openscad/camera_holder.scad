cam_slot_w = 9.2;
cam_slot_th = 3.7;
cam_slot_opening_w = cam_slot_w - .8 * 2;
cam_slot_depth = 4;

wall = 1.4;

module camera_holder(extra_front_len = 0, extra_h = 0) {
  union() {
    difference() {
      cube([cam_slot_w + wall*2, cam_slot_th + wall*2 + extra_front_len, cam_slot_depth + wall*2 + extra_h]);
      translate([wall, wall + extra_front_len, wall]) cube([cam_slot_w, cam_slot_th, cam_slot_depth*2 + extra_h]);
      translate([wall + (cam_slot_w - cam_slot_opening_w)/2, -wall, wall]) cube([cam_slot_opening_w, wall*3 + extra_front_len, cam_slot_depth*2 + extra_h]);
    }
  }
}

function camera_holder_x() = cam_slot_w + wall*2;
function camera_holder_y() = cam_slot_th + wall*2;
function camera_holder_z() = cam_slot_depth + wall*2;
function camera_holder_wall() = wall;

camera_holder();
