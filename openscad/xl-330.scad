// servo dimensions

xl_330_w = 20;
xl_330_h = 34;
xl_330_d = 23;
xl_330_horn_dia = 16;
xl_330_horn_h = 3;
xl_330_horn_z_pos = 24.5;
xl_330_horn_holes_dist = 6;
xl_330_horn_holes_dia = 1.6;
xl_330_mounting_holes_dist = 2;
xl_330_opposite_outer_hole_dia = 7;
xl_330_opposite_outer_hole_d = 1.2;
xl_330_opposite_inner_hole_dia = 2;
xl_330_opposite_inner_hole_d = 4;
xl_330_conn_z_pos = 10;
xl_330_conn_w = 4;
xl_330_conn_h = 11;
xl_330_conn_d = 9;


// commonly used dimensions for joints/brackets

xl_330_joint_bracket_y_gap = 0.2;
xl_330_joint_outer_dia = xl_330_horn_dia + 2.8;
xl_330_joint_horn_dia_extend = 0.4;  
xl_330_joint_d = xl_330_horn_h + 2;

xl_330_opposite_joint_d = 2;
xl_330_opposite_joint_dia = xl_330_opposite_outer_hole_dia + 4;

xl_330_bracket_thickness = 1;

xl_330_bracket_gap = xl_330_joint_bracket_y_gap;

xl_330_link_w = 8;
xl_330_link_d = xl_330_opposite_joint_d;
xl330_link_sides_connector_w = 5;
xl330_link_sides_connector_w_double = xl_330_link_w + 6;
xl330_link_sides_connector_h = 4;


module xl_330_hull(extend = 0, horn_dia_extend = 0, horn_h_extend = 0) {
  w = xl_330_w + extend;
  d = xl_330_d + extend;
  h = xl_330_h + extend;
  cube([w, d, h]);
  translate([w/2, d + xl_330_horn_h, xl_330_horn_z_pos + extend/2]) rotate([90,0,0]) cylinder(d=xl_330_horn_dia + horn_dia_extend, h=xl_330_horn_h + horn_h_extend);  
}

module xl_330_horn_holes(h, dia = xl_330_horn_holes_dia) {
  translate([xl_330_horn_holes_dist, 0, 0]) cylinder(d = dia, h = h);
  translate([-xl_330_horn_holes_dist, 0, 0]) cylinder(d = dia, h = h);
  translate([0, xl_330_horn_holes_dist, 0]) cylinder(d = dia, h = h);
  translate([0, -xl_330_horn_holes_dist, 0]) cylinder(d = dia, h = h);
}

module xl_330_mounting_holes(h = xl_330_d+2, dia = xl_330_horn_holes_dia) {
  translate([xl_330_mounting_holes_dist, -1, xl_330_mounting_holes_dist]) {
    rotate([-90,0,0]) cylinder(h = h, d = dia);
  }
  translate([xl_330_w - xl_330_mounting_holes_dist, -1, xl_330_mounting_holes_dist]) {
    rotate([-90,0,0]) cylinder(h = h, d = dia);
  }
  translate([xl_330_mounting_holes_dist, -1, xl_330_h - xl_330_mounting_holes_dist]) {
    rotate([-90,0,0]) cylinder(h = h, d = dia);
  }
  translate([xl_330_w - xl_330_mounting_holes_dist, -1, xl_330_h - xl_330_mounting_holes_dist]) {
    rotate([-90,0,0]) cylinder(h = h, d = dia);
  }
}

module xl_330() {
  difference() {
    xl_330_hull();
    translate([xl_330_w/2, xl_330_d + 0, xl_330_horn_z_pos]) rotate([-90,0,0]) xl_330_horn_holes(xl_330_horn_h + 1);
    translate([xl_330_w/2, xl_330_opposite_inner_hole_d, xl_330_horn_z_pos]) rotate([90,0,0]) cylinder(d = xl_330_opposite_inner_hole_dia, h = xl_330_opposite_inner_hole_d + 1);
    translate([xl_330_w/2, xl_330_opposite_outer_hole_d, xl_330_horn_z_pos]) rotate([90,0,0]) cylinder(d = xl_330_opposite_outer_hole_dia, h = xl_330_opposite_outer_hole_d + 1);
    translate([-1, -1, xl_330_conn_z_pos]) cube([xl_330_conn_w + 1, xl_330_conn_d + 1, xl_330_conn_h]);
    translate([xl_330_w - xl_330_conn_w, -1, xl_330_conn_z_pos]) cube([xl_330_conn_w + 1, xl_330_conn_d + 1, xl_330_conn_h]);
    xl_330_mounting_holes();
  }
}


