include <xl-330.scad>
use <xl-330-base.scad>

xl_330_base_bottom();
translate([0, 0, xl_330_base_bottom_total_height()]) {
  xl_330_base_cover();
}

translate([-xl_330_w / 2, xl_330_horn_z_pos, xl_330_base_servo_z_offset()]) {
  rotate([90,0,0]) xl_330();
}

echo("base_height", (xl_330_base_bottom_total_height() + xl_330_base_cover_total_height()) / 1000);

