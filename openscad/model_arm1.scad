$fn = 50;

include <xl-330.scad>
use <xl-330-brackets.scad>
use <xl-330-bottom-bracket.scad>

l = 60;

translate([0, -xl_330_d/2, 0]) {
  xl_330_joint_platform_opposite_side(l = l);
  xl_330_joint_platform_horn_side(l = l);
  translate([-xl_330_w/2, 0, l + xl_330_bracket_thickness]) {
    xl_330_bottom_bracket(add_hooks = true);
    xl_330();
  }
}

echo("arm1_length", (l + xl_330_bracket_thickness + xl_330_horn_z_pos) / 1000);

