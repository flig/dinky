// The apple tree
use <xl-330-base.scad>

$fn=50;

h = 400;
trunk_w = 70/2;
top_h = 50;
top_w = 85;
mid_h = 50;
mid_w = 140;
bottom_h = 55;
bottom_w = 195;
gap = 70;
th = 5;
base_h = 60;
base_w = 70;

h_sit = 70; // Flig's height when sitting
base_height = xl_330_base_bottom_total_height() + xl_330_base_cover_total_height();
z_offset = -h_sit - base_height;

apple_d = 40;
apple_x = [top_w - apple_d/2, mid_w - apple_d/2, bottom_w - apple_d/2];
apple_z = [h - top_h - apple_d/2, h - top_h - gap - mid_h - apple_d/2, h - top_h - gap - mid_h - gap - bottom_h - apple_d/2];
apple_pos = concat(
  [for (i = [0:2]) [apple_x[i], 0, apple_z[i] + z_offset]],
  [for (i = [0:2]) [-apple_x[i], 0, apple_z[i] + z_offset]],
  [for (i = [0:2]) [0, apple_x[i], apple_z[i] + z_offset]],
  [for (i = [0:2]) [0, -apple_x[i], apple_z[i] + z_offset]]
);

module tree() {
  module half() {
    translate([0, -th/2, 0]) {
      cube([trunk_w, th, h]);
      translate([0, 0, h - top_h]) cube([top_w, th, top_h]);
      translate([0, 0, h - top_h - gap - mid_h]) cube([mid_w, th, mid_h]);
      translate([0, 0, h - top_h - gap - mid_h - gap - bottom_h]) cube([bottom_w, th, bottom_h]);
    }
  }

  color("RosyBrown") {
    for (a=[0, 90, 180, 270]) {
      rotate(a) half();
    }
    translate([-base_w/2, -base_w/2, 0]) cube([base_w, base_w, base_h]);
  }
}

module apples() {
  color("Red") {
    for (p = apple_pos) {
      translate(p) sphere(d=apple_d);
    }
  }
}

translate([0, 0, z_offset]) tree();
//translate([0, 0, -h_sit - base_height]) apples();

echo("apple positions:", apple_pos);
