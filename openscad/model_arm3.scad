$fn = 50;

include <xl-330.scad>
use <xl-330-brackets.scad>
use <xl-330-bottom-bracket.scad>

l = min_xl_330_link_len();

translate([0, -xl_330_d/2, 0]) {
  xl_330_joint_platform_opposite_side(l = l);
  xl_330_joint_platform_horn_side(l = l);
  offs = -(xl_330_w - xl_330_d)/2;
  translate([-xl_330_d/2, offs, l + xl_330_bracket_thickness]) {
    xl_330_bottom_bracket_90deg();
    translate([xl_330_d, 0*xl_330_w, 0]) rotate(90) xl_330();
  }
}

echo("arm3_length", (l + xl_330_bracket_thickness + xl_330_horn_z_pos) / 1000);


