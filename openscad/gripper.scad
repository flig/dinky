$fn=50;

include <./common.scad>
include <xl-330.scad>

use <xl-330-bottom-bracket.scad>
use <xl-330-brackets.scad>
use <camera_holder.scad>

inner_d = 40;
thickness = 3.2;
outer_d = inner_d + thickness;
h = 24;
ring_w = 5.5;
ring_thickness = 2;
hinge_offs = 3.5;
hinge_d = 5;
hinge_hole_d = 1.8;

horizontal = true; // gripper servo sitting horizontally (vertical version isn't complete - probably not needed)

horiz_servo_holder_x_gap = xl_330_bracket_gap + 0.2;
horiz_servo_holder_wall_thickness = 1.4;

base_yoffs = -8;
base_zoffs = xl_330_w/2 + horiz_servo_holder_wall_thickness + horiz_servo_holder_x_gap;  // z offset from servo horn center height

arm_conn_len = xl_330_d + 1;

gap_above_horn_link = 2;
bottom_link_l = min_xl_330_link_len() + 3;
gripper_servo_arm_len = min_xl_330_link_len() + 11;

gripper_base_top_center_yoffs = -outer_d / 2 - 9;

// one section of the gripper
module section() {
  difference() {
    union() {
      intersection() {
        difference() {
          sphere(d = outer_d);
          sphere(d = inner_d);
        }
        cube([outer_d + 2, outer_d + 2, h], center = true);  
      }
      groove_d = 6;
      groove_offs = 0;
      difference() {
        union() {
          rotate_extrude(convexity = 2) translate([outer_d / 2, 0, 0]) circle(r = ring_w);
          // ring for outer groove
          difference() {
            translate([0, 0, -ring_w / 2 - 1]) cylinder(d = outer_d + ring_w * 2 + groove_offs * 2, h = ring_w+2);
          }
        }
        rotate_extrude(convexity = 2) translate([outer_d / 2, 0, 0]) circle(r = ring_w - ring_thickness);
        sphere(d = outer_d);
        // outer groove
        rotate_extrude(convexity = 2) translate([outer_d / 2 + ring_w + groove_offs + 1, 0, 0]) circle(d = groove_d -2);
      }
    }
    rotate(15) translate([-outer_d, 0, -outer_d / 2 - 1]) cube([outer_d * 2, outer_d, outer_d + 2]);
    mirror([0,1,0]) rotate(15) translate([-outer_d, 0, -outer_d / 2 - 1]) cube([outer_d * 2, outer_d, outer_d + 2]);
  }
}

module hinge(cutout = false, upper = true) {
  d = cutout ? hinge_d + 0.4 : hinge_d;

  module lower_part() {
    lower_h = ring_w - (cutout ? -1 : 0.1);
    rotate(cutout ? 5 : 25) {
      cylinder(d = d, h = lower_h);
      translate([0, -d / 2, 0]) cube([cutout ? d : d/2, cutout ? d : d/2, lower_h]);
    }
  }

  module upper_part() {
    upper_h = ring_w - (cutout ? -0.1 : 0.1);
    rotate(cutout ? -5 : 65) {
      cylinder(d = d, h = upper_h);
      translate([0, -d / 2, 0]) cube([cutout ? d : d/2, cutout ? d : d/2, upper_h]);
    }
  }
  
  if (upper) {
    translate([outer_d/2 + hinge_offs, 0, cutout ? -0.1 : 0.1]) {
      upper_part();
    }
  } else {
    translate([outer_d/2 + hinge_offs, 0, -ring_w + (cutout ? -1+0.1 : 0)]) {
      lower_part();
    }
  }
}

module hinge_hole() {
  translate([outer_d/2 + hinge_offs, 0, -ring_w - 1]) cylinder(d = hinge_hole_d, h = ring_w * 2 + 2);
}

module section_w_hinge(start_hinge = true, end_hinge = true, rotate_at_start = 0) {
  tr = rotate_at_start == 0 ? [0, 0, 0] : [outer_d/2 + hinge_offs, 0, 0];
  rotate(-15) translate(tr) rotate(rotate_at_start) translate(-tr) rotate(15) {
    difference() {
      union() {
        difference() {
          section();
          if (start_hinge) rotate(-15) hinge(upper = true, cutout = true);
          if (end_hinge) rotate(15) hinge(upper = false, cutout = true);
        }
        if (end_hinge) rotate(15) hinge(upper = true);
        if (start_hinge) rotate(-15) hinge(upper = false);
      }
      if (end_hinge) rotate(15) hinge_hole();
      if (start_hinge) rotate(-15) hinge_hole();
    }
  }
}

module base_start_section(l = 10) {
  hinge_pos = [-outer_d/2 + ring_w/2, l, 0];
  hh = ring_w * 2;
  difference() {
    union() {
      translate([ring_thickness, 0, 0]) {
        rotate([-90, 90, 0]) {
          difference() {
            translate([-ring_w, -ring_w, 0]) cube([ring_w*2, ring_w, l]);
            translate([0, 0, -1]) cylinder(r = ring_w - ring_thickness, h = l + 2);
            translate([-ring_w - 1, 0, -1]) cube([ring_w * 2 + 2, ring_w + 1, l + 2]);
          }
          translate([-hh / 2, 0, 0]) cube([hh, ring_thickness, l]);
        }
      }
      translate(hinge_pos) hinge(upper = true);
    }
    translate(hinge_pos) {
      hinge_hole();
      hinge(upper = false, cutout = true);
    }
    translate([ring_w + ring_thickness - hinge_offs - 2, l + 0, -hh / 2 - 1]) rotate(70) translate([-ring_w - 10, 0, 0]) cube([ring_w + 10, 10, hh + 2]);
  }
}


module horiz_servo_holder() {
  bottom_bracket_h = xl_330_bottom_bracket_h(wall_thickness = horiz_servo_holder_wall_thickness);
  
  d = xl_330_bottom_bracket_inner_d(wall_thickness = horiz_servo_holder_wall_thickness, gap = horiz_servo_holder_x_gap);
  top_end_w = 8;
  top_w = horiz_servo_holder_wall_thickness + horiz_servo_holder_x_gap + xl_330_h + horiz_servo_holder_x_gap + top_end_w - bottom_bracket_h;
  bottom_w = xl_330_h + (horiz_servo_holder_wall_thickness + horiz_servo_holder_x_gap) * 2 - bottom_bracket_h - horiz_servo_holder_wall_thickness;
  h = xl_330_bottom_bracket_w(wall_thickness = horiz_servo_holder_wall_thickness, gap = horiz_servo_holder_x_gap);

  module move_servo() {
    translate([xl_330_d / 2, -horiz_servo_holder_x_gap, xl_330_w]) rotate([0,90,90]) {
      children();
    }
  }

  module cutout() {
      // cut out main servo shape + top
      xl_330_hull(extend = horiz_servo_holder_x_gap);
      translate([-20, 0, 0]) xl_330_hull(extend = horiz_servo_holder_x_gap);

      // cut out sides
      round_cutout_d = (xl_330_h - xl_330_horn_z_pos + horiz_servo_holder_x_gap) * 2;
      hull() {
        translate([xl_330_w/2, xl_330_d + 10, xl_330_horn_z_pos]) rotate([90, 0, 0]) cylinder(d = round_cutout_d, h = xl_330_d+20);
        translate([xl_330_w/2, xl_330_d + 10, xl_330_horn_z_pos - 8]) rotate([90, 0, 0]) cylinder(d = round_cutout_d, h = xl_330_d+20);
        translate([xl_330_w/2 - round_cutout_d/2 + 2.5, xl_330_d + 10, bottom_bracket_h + 2.5]) rotate([90, 0, 0]) cylinder(d = 5, h = xl_330_d+20);
        translate([xl_330_w/2 - round_cutout_d/2 + 2.5, xl_330_d + 10, xl_330_horn_z_pos + round_cutout_d/2 - 2.5]) rotate([90, 0, 0]) cylinder(d = 5, h = xl_330_d+20);
      }
      translate([xl_330_w/2, -10, xl_330_horn_z_pos - 8 - round_cutout_d/2] ) cube([round_cutout_d / 2, xl_330_d + 20, round_cutout_d / 2]);
      translate([-round_cutout_d / 2, 0, xl_330_horn_z_pos - 8- round_cutout_d/2] ) cube([round_cutout_d, xl_330_d + 10, round_cutout_d+8]);
      
      // cut out front
      dd = xl_330_w - 2;
      difference() {
        hull() {
          translate([xl_330_w / 2 - 7, xl_330_d / 2 - dd/2 + 3, 0]) cylinder(d = 5, h = xl_330_h + 10);
          translate([xl_330_w / 2 - 7, xl_330_d / 2 + dd/2 - 3, 0]) cylinder(d = 5, h = xl_330_h + 10);

          translate([xl_330_w / 2 - 3 + dd / 2, xl_330_d / 2 - dd/2 + 3, 0]) cylinder(d = 5, h = xl_330_h + 10);
          translate([xl_330_w / 2 - 3 + dd / 2, xl_330_d / 2 + dd/2 - 3, 0]) cylinder(d = 5, h = xl_330_h + 10);
        }
      }

      // cut out from floor
      translate([xl_330_w, xl_330_d - 9.5  - dd/2*0, 0]) cube([10, 8.5, xl_330_h]);
      translate([xl_330_w, 1  - dd/2*0, 0]) cube([10, 8.5, xl_330_h]);

      // cut out hole for cam cable
      translate([-2, xl_330_d/2 - 6, 0]) cube([5, 12, xl_330_h + 3]);
  }
  
  difference() {
    union() {
      translate([-d / 2, bottom_bracket_h - horiz_servo_holder_wall_thickness - horiz_servo_holder_x_gap, -horiz_servo_holder_wall_thickness - horiz_servo_holder_x_gap]) {
        hull() {
          cube([d, bottom_w, 0.1]);          
          translate([0, 0, h - 0.1]) cube([d, top_w, 0.1]);
        }
        // bottom screw hole posts
        translate([-5, 0, 0]) cube([5, 7, 2]);
        translate([d, 0, 0]) cube([5, 7, 2]);
      }
      move_servo() {
        xl_330_bottom_bracket(nut_opening_angle = -90, wall_thickness = horiz_servo_holder_wall_thickness, gap = horiz_servo_holder_x_gap);
        %xl_330();
      }
    }
    
    move_servo() {
      cutout();
    }
    translate([0, xl_330_h - base_yoffs, base_zoffs + ring_w + xl_330_w / 2]) _base_top_screw_holes();
    // bottom screw hole posts
    translate([-d/2 - 2, 8, -5]) cylinder(d = m2_screw_hole_d, h = 10);
    translate([d/2 + 2, 8, -5]) cylinder(d = m2_screw_hole_d, h = 10);
  }

  translate([-camera_holder_x() / 2, xl_330_h + camera_holder_y(), -horiz_servo_holder_wall_thickness - horiz_servo_holder_x_gap]) {
    mirror([0, 1, 0]) camera_holder();
  }
}


module _base_top_screw_holes() {
  base_top_screw_hole_pos = [xl_330_bottom_bracket_inner_d() / 2 - 3, base_yoffs + 3, -10];
  //TODO in xl-330-base.scad: post_hole_d = 2.2 (we need slightly bigger apparently)
  translate(base_top_screw_hole_pos) cylinder(d = 2.4, h = 20);
  mirror([1,0,0]) translate(base_top_screw_hole_pos) cylinder(d = 2.4, h = 20);
}


function camera_holder_offset() = [0, gripper_base_top_center_yoffs + camera_holder_y() / 2 + 1, ring_thickness];

module gripper_base_top() {
  w = xl_330_bottom_bracket_inner_d(wall_thickness = horiz_servo_holder_wall_thickness, gap = horiz_servo_holder_x_gap);
  post_x = -12;
  post_y = 1;
  gripper_base_top_opening_angle = 37.5;
    
  for (i = [0, 1]) {
    mirror([i, 0, 0]) {
      rotate(-gripper_base_top_opening_angle) translate([0,12,0]) base_start_section(l = 9);
    }
  }
  
  translate([0, 0, -ring_w]) {
    difference() {
      union() {
        rotate(-gripper_base_top_opening_angle) cube([ring_w + ring_thickness, 12, ring_thickness]);
        mirror([1, 0, 0]) {
          rotate(-gripper_base_top_opening_angle) cube([ring_w + ring_thickness, 12, ring_thickness]);
        }

        plate_d = -(base_yoffs + xl_330_bracket_gap);
        translate([-w / 2, -plate_d, 0]) {
          difference() {
            cube([w, plate_d, ring_thickness]);
            translate([(w - 12)/2, -1, -1]) cube([12, 3, 4]);
          }
        }
        translate([post_x, post_y, 0]) cylinder(d = 5, h = ring_w*2);
        translate([-post_x, post_y, 0]) cylinder(d = 5, h = ring_w*2);

      }
      translate([post_x, post_y, -1]) cylinder(d = hinge_hole_d, h = ring_w*2 + 2);
      translate([-post_x, post_y, -1]) cylinder(d = hinge_hole_d, h = ring_w*2 + 2);

      _base_top_screw_holes();
    }
    
    translate(camera_holder_offset() + [-camera_holder_x() / 2, -gripper_base_top_center_yoffs, 0]) {
      mirror([0, 1, 0]) camera_holder();
    }
  }
}

module bottom_horn_link() {
  rotate([90,0,180]) translate([0, -xl_330_d - xl_330_joint_bracket_y_gap - gap_above_horn_link, 0]) {
    difference() {
      xl_330_joint_horn_link(l = bottom_link_l, thin_top = true, rounded_end = true);
      servo_arm_square_indent(bottom_link_l);
    }
  }
  
  translate([0, 0, xl_330_joint_d - gap_above_horn_link]) {
    difference() {
      cylinder(d = xl_330_joint_outer_dia, h = gap_above_horn_link);
      translate([0, 0, -0.001]) cylinder(d = xl_330_joint_outer_dia - 2, h = gap_above_horn_link + 1);
    }
  }
  translate([0, 0, xl_330_joint_d]) {
    difference() {
      wall = horiz_servo_holder_wall_thickness;
      post_y = xl_330_bottom_bracket_h(wall_thickness = horiz_servo_holder_wall_thickness);
      w1 = xl_330_bottom_bracket_d(wall_thickness = horiz_servo_holder_wall_thickness, gap = horiz_servo_holder_x_gap);
      w2 = w1 + wall * 2 + horiz_servo_holder_x_gap * 2;
      w3 = xl_330_bottom_bracket_inner_d(wall_thickness = horiz_servo_holder_wall_thickness, gap = horiz_servo_holder_x_gap);
      d1 = post_y;
      d2 = 7;
      d3 = xl_330_joint_outer_dia - d1 - d2;
      dd = xl_330_joint_outer_dia + horiz_servo_holder_wall_thickness + horiz_servo_holder_x_gap;
      yoffs = -xl_330_joint_outer_dia / 2;
      hull() {
        translate([-w1 / 2, yoffs, -horiz_servo_holder_wall_thickness]) {
          cube([w1, d1, horiz_servo_holder_wall_thickness]);
        }
        translate([-w2 / 2, yoffs + post_y, -horiz_servo_holder_wall_thickness]) {
          cube([w2, d2, horiz_servo_holder_wall_thickness]);
        }
        translate([-w3 / 2, yoffs + post_y + 7, -horiz_servo_holder_wall_thickness]) {
          cube([w3, d3, horiz_servo_holder_wall_thickness]);
        }
      }
      translate([0, 0, -horiz_servo_holder_wall_thickness - 1]) {
        cylinder(d = xl_330_joint_outer_dia, h = horiz_servo_holder_wall_thickness + 2);
      }
      // bottom screw holes
      //TODO magic numbers...
      translate([-w1/2 + 1.2, 0.3, -horiz_servo_holder_wall_thickness - 1]) {
        cylinder(d = m2_screw_hole_d, h = 10);
      }
      translate([w1/2 - 1.2, 0.3, -horiz_servo_holder_wall_thickness - 1]) {
        cylinder(d = m2_screw_hole_d, h = 10);
      }
    }
  }
}

module bottom_opposite_link() {
  rotate([90,0,180]) translate([0, -xl_330_d - xl_330_joint_bracket_y_gap - gap_above_horn_link, 0]) difference() {
    xl_330_joint_opposite_link(l = bottom_link_l, rounded_end = true);
    servo_arm_square_indent(bottom_link_l);
  }
}

module servo_arm_square_indent(arm_len) {
  w = 4.5;
  translate([0, 0, arm_len]) {
    translate([-w/2, -xl_330_joint_bracket_y_gap - 0.5, -w/2]) cube([w, arm_conn_len + xl_330_joint_bracket_y_gap * 2, w]);
    rotate([90, 0, 0]) translate([0, 0, -xl_330_d-10]) cylinder(d = m2_screw_hole_d, h = xl_330_d * 2);
  }
}

module gripper_servo_arm(opposite = false, arm_len = gripper_servo_arm_len) {
  difference() {
    if (opposite) {
      xl_330_joint_opposite_link(l = arm_len, rounded_end = true, clear_cable = true, w_offset = 10);
    } else {
      xl_330_joint_horn_link(l = arm_len, thin_top = true, rounded_end = true);
    }
    servo_arm_square_indent(arm_len);
  }
}

module gripper_servo_arm_connector() {
  rotate([90, 0, 0]) difference() {
    translate([-2, 0, -2]) {
      cube([4, arm_conn_len, 4]);
      translate([0, 5, 0]) cylinder(d=5, h=4);
      translate([0, arm_conn_len - 5, 0]) cylinder(d=5, h=4);
    }
    rotate([90, 0, 0]) translate([0, 0, -arm_conn_len - 1]) cylinder(d = m2_screw_hole_d, h = arm_conn_len + 2);
    translate([-2, 5, -4]) cylinder(d = m2_screw_hole_d, h = 8);
    translate([-2, arm_conn_len - 5, -4]) cylinder(d = m2_screw_hole_d, h = 8);
  }
}

// gripper origin = center of sphere in gripper
// bottom servo = origin of servo below us
function gripper_origin_to_bottom_servo_offs() = [
 0,
 base_yoffs - xl_330_horn_z_pos - horiz_servo_holder_wall_thickness - horiz_servo_holder_x_gap + gripper_base_top_center_yoffs,
 -xl_330_d/2 - ring_w - horiz_servo_holder_wall_thickness*2 - horiz_servo_holder_x_gap * 2 - xl_330_joint_d - xl_330_w - gap_above_horn_link
];

module gripper_demo(servo = false, rot = 0) {
  %sphere(d = inner_d);

  module bottom_servo_demo() {
    translate([xl_330_w/2, -xl_330_horn_z_pos, -xl_330_d/2]) rotate([90,0,180]) {
      %xl_330();
      %xl_330_bottom_bracket();
    }
  }

  translate([0, gripper_base_top_center_yoffs, 0]) {

    translate([0, 0, 0.2]) gripper_base_top();
  
    translate([0, base_yoffs, -base_zoffs - ring_w - (horizontal ? xl_330_w / 2 : xl_330_horn_z_pos)]) {
      if (horizontal) {
        translate([0, -xl_330_h, 0]) {
          translate([-arm_conn_len/2, xl_330_horn_z_pos, xl_330_w / 2 + gripper_servo_arm_len]) rotate([0, 90, 0]) gripper_servo_arm_connector();
          horiz_servo_holder();
          translate([xl_330_d / 2, 0, xl_330_w]) rotate([0, 90, 90]) {
            if (servo) {
              xl_330();
            } else {
              %xl_330();
            }
          }
        }
      } else {
        translate([xl_330_d / 2, -xl_330_w, 0]) {
          rotate([0, 0, 90]) {
            xl_330_bottom_bracket();
            %xl_330();
          }
        }
      }
    }
    
    translate(gripper_origin_to_bottom_servo_offs() - [0, gripper_base_top_center_yoffs, 0.2+0.1]) {
      translate([0, 0, xl_330_d/2 + xl_330_joint_bracket_y_gap + gap_above_horn_link]) {
        bottom_horn_link();
        bottom_opposite_link();
        translate([0, bottom_link_l, -xl_330_d - gap_above_horn_link - xl_330_joint_bracket_y_gap*2]) xl_330_link_sides_connector();
      }
      rotate(rot) bottom_servo_demo();
    }
  }

  translate([0, base_yoffs + gripper_base_top_center_yoffs - xl_330_w, -ring_w - base_zoffs]) {
    translate([xl_330_d / 2, xl_330_w / 2 + xl_330_joint_bracket_y_gap*2, 0]) rotate([0, 0, 90]) {    
      gripper_servo_arm();
      gripper_servo_arm(opposite = true);
    }
  }
}

module gripper_demo_closed() {
  for (i = [0, 1]) {
    mirror([i, 0, 0]) {
      rotate(-30.2) section_w_hinge(start_hinge = true, end_hinge = true);
      section_w_hinge(start_hinge = true, end_hinge = true);
      rotate(30.2) section_w_hinge(start_hinge = true, end_hinge = true);
    }
  }
}

module gripper_demo_open() {
  for (i = [0, 1]) {
    mirror([i, 0, 0]) {
      rotate(-30.2) section_w_hinge(start_hinge = true, end_hinge = true, rotate_at_start = -15);
      translate([3, -2, 0]) section_w_hinge(start_hinge = true, end_hinge = true, rotate_at_start = -30);
      translate([10, -4, 0]) rotate(30.2) section_w_hinge(start_hinge = true, end_hinge = true, rotate_at_start = -45);
    }
  }
}

module gripper_demo_wide_open() {
  for (i = [0, 1]) {
    mirror([i, 0, 0]) {
      rotate(-30.2) section_w_hinge(start_hinge = true, end_hinge = true, rotate_at_start = -30);
      translate([4.7, -4.6, 0]) section_w_hinge(start_hinge = true, end_hinge = true, rotate_at_start = -60);
      translate([16.1, -11, 0]) rotate(30.2) section_w_hinge(start_hinge = true, end_hinge = true, rotate_at_start = -90);
    }
  }
}

//----------------------------------------------------------------------------------
// Demo
//----------------------------------------------------------------------------------

gripper_demo(rot = -45);
gripper_demo_wide_open();
//gripper_demo_open();
//gripper_demo_closed();

//----------------------------------------------------------------------------------
// Print
//----------------------------------------------------------------------------------

//for (i=[0 : 15 : 40]) translate([i, 0, 0]) section_w_hinge(start_hinge = true, end_hinge = true);
//mirror([0, 0, 1]) for (i=[0 : 15 : 40]) translate([i, 0, 0]) section_w_hinge(start_hinge = true, end_hinge = true);

//gripper_servo_arm();
//gripper_servo_arm(opposite = true);

//rotate([90, 0, 0]) gripper_servo_arm_connector();

//bottom_horn_link();
//bottom_opposite_link();

//horiz_servo_holder();

//gripper_base_top();
