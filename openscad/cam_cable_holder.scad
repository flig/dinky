$fn=50;

include <./common.scad>
include <xl-330.scad>
use <gripper.scad>
use <xl-330-brackets.scad>

gripper_cam_cable_holder_h = xl_330_w + 30;

module cam_cable_holder_plate() {
  cube([20, 8, 0.5]);
}

module cable_holder_col(h, thick = false) {
  cube([4, thick ? 2 : 0.8, h]);
  hull() {
    translate([0, 0, h - 0.4]) cube([4, thick ? 8 : 4, 0.5]);
    translate([0, 0, h - 4 - 0.4]) cube([4, 0.1, 0.1]);
  }
}

module gripper_cam_cable_holder() {
  h = gripper_cam_cable_holder_h;
  translate([-10, 0, 0]) {    
    translate([4, 0, 0]) cable_holder_col(h);
    translate([20 - 8, 0, 0]) cable_holder_col(h);
    translate([4, 0, h - 4]) cube([20 - 8, 0.8, 4]);
  
    %translate([0, 0, h + 0.2]) cube([20, 20, 0.5]);
  }
}

//----------------------------------------------------------------------------------
// Demo
//----------------------------------------------------------------------------------

module demo1() {
  horiz_servo_holder();
  translate([0, -3.2, 0]) {
    gripper_cam_cable_holder();
    translate([-10, 0, gripper_cam_cable_holder_h - 0.4]) cam_cable_holder_plate();
  }
}

module demo2() {
  xl_330_joint_platform_opposite_side(l = 60);
  xl_330_joint_platform_horn_side(l = 60);

  translate([-5, 0, 45]) rotate([0, 90, 0]) cable_holder_col(30, thick=true);
  translate([25, 0, 40]) rotate([90, 0, 90]) cam_cable_holder_plate();
}

demo1();
//demo2();


//----------------------------------------------------------------------------------
// Print
//----------------------------------------------------------------------------------
//rotate([0,90,0]) cable_holder_col(30, thick=true);
//cam_cable_holder_plate();
//rotate([90,0,0]) gripper_cam_cable_holder();
