m2_screw_hole_d = 2.6;
m2_nut_d = 4.6;
m2_nut_h = 1.5 + 0.2;
m2_locknut_h = 2.8;
m2_nut_flat_w = 4;

// 6804zz bearing sizes
bearing_id = 19.8; //20;
bearing_od = 32.2; //32;
bearing_h = 7;
bearing_inner_od = 27; //24;

module m2_nut_cutout(h=10, locknut=false, expand_h = 0) {
  nut_h = (locknut ? m2_locknut_h : m2_nut_h) + expand_h;
  rotate([-90,0,0]) {
    translate([-(m2_nut_flat_w+0.4)/2, -h-2, 0]) cube([m2_nut_flat_w+0.4, h+2, nut_h]);
    rotate([0,0,30]) cylinder(d=m2_nut_d+0.4, $fn=6, h=nut_h);
  }
}
