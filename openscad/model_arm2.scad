$fn = 50;

include <xl-330.scad>
use <xl-330-brackets.scad>
use <xl-330-bottom-bracket.scad>

l = 60;
bend_angle = -45;

translate([0, -xl_330_d/2, 0]) {
  rotate([0, -45, 0]) xl_330_joint_platform_opposite_side_lower(l = l);
  xl_330_joint_platform_opposite_side_upper(l = l, bend_angle = bend_angle);
  xl_330_joint_platform_horn_side(l = l, bend_angle = bend_angle);
  translate([-xl_330_w/2 + offset_for_bend_angle(bend_angle), 0, l + xl_330_bracket_thickness]) {
    xl_330_bottom_bracket(add_hooks = true);
    xl_330();
  }
}

echo("arm2_length", (l + xl_330_bracket_thickness + xl_330_horn_z_pos) / 1000);
echo("arm2_offset", offset_for_bend_angle(bend_angle) / 1000);

