include <./common.scad>
include <./xl-330.scad>
use <./xl-330-bottom-bracket.scad>

$fn=100;

top_cover_plate_thickness = 2;

horn_connector_hex_conn_gap = top_cover_plate_thickness + 0.8;

horn_connector_hex_conn_dia = bearing_inner_od - 3;
horn_connector_h = xl_330_joint_d + horn_connector_hex_conn_gap;

bearing_top_gap = 0.4;

bottom_base_holes_dist = 45 / 2;
bottom_base_holes_outer_dia = 8;
bottom_base_holes_inner_dia = 4.2;
bottom_base_holes_h = 3;
bottom_base_h = bottom_base_holes_h * 2;
bottom_base_holes_pos = [ [-bottom_base_holes_dist, -bottom_base_holes_dist, 0],
                          [bottom_base_holes_dist, bottom_base_holes_dist, 0],
                          [-bottom_base_holes_dist, bottom_base_holes_dist, 0],
                          [bottom_base_holes_dist, -bottom_base_holes_dist, 0] ];

bottom_base_w = bottom_base_holes_dist * 2 + bottom_base_holes_outer_dia + 4;

// connector to Flig's top attachment holes: version 1
module plug() {
  bottom_h = bottom_base_holes_h + 4;
  reduce = 0.2;
  bottom_dia_reduce = 0.5;
  difference() {
    union() {
      cylinder(d1 = bottom_base_holes_inner_dia - reduce - bottom_dia_reduce, d2 = bottom_base_holes_inner_dia - reduce, h = bottom_h);
      translate([0, 0, bottom_h]) cylinder(d = bottom_base_holes_outer_dia - reduce, h = bottom_base_holes_h);
      cylinder(d = bottom_base_holes_inner_dia - reduce, h = 1);
    }
    translate([0, 0, bottom_h - 0.2]) cylinder(d = 2.2, h = 10);
    translate([0, 0, -0.001]) cylinder(d1 = 2.2 - bottom_dia_reduce, d2 = 2.2, h = bottom_h);
    translate([0, 0, bottom_h + bottom_base_holes_h - m2_nut_h - 0.2]) rotate(90) cylinder(d = m2_nut_d, h = 10, $fn=6);
    slot_w = 1.4;
    hull() {
      translate([-(slot_w - bottom_dia_reduce) / 2, -10, -0.1]) cube([slot_w - bottom_dia_reduce, 20, 0.1]);
      translate([-slot_w / 2, -10, bottom_h - 0.1001]) cube([slot_w, 20, 0.1]);
    }
    translate([-5, (bottom_base_holes_inner_dia - reduce)/2 - 0.6, -0.001]) cube([10, 5, bottom_h]);
    translate([-5, -5-(bottom_base_holes_inner_dia - reduce)/2 + 0.6, -0.001]) cube([10, 5, bottom_h]);
    translate([-5, bottom_base_holes_outer_dia/2 - 1, 0]) cube([10, 5, 20]);
  }
}

// connector to Flig's top attachment holes: version 2 with extra spacer
module plug2(reduce = 0.2) {
  spacer_h = 6;
  spacer_d = 10;
  bottom_h = bottom_base_holes_h + 4;
  bottom_dia_reduce = 0.5;
  difference() {
    union() {
      cylinder(d = bottom_base_holes_inner_dia - reduce, h = 1);
      cylinder(d1 = bottom_base_holes_inner_dia - reduce - bottom_dia_reduce, d2 = bottom_base_holes_inner_dia - reduce, h = bottom_h);
      translate([0, 0, bottom_h]) cylinder(d = bottom_base_holes_outer_dia - reduce, h = bottom_base_holes_h);
      color("Gray") translate([0, 0, bottom_h + bottom_base_holes_h]) cylinder(d = spacer_d, h = spacer_h);
    }
    translate([0, 0, bottom_h - 0.2]) cylinder(d = 2.2, h = 10);
    translate([0, 0, -0.001]) cylinder(d1 = 2.2 - bottom_dia_reduce, d2 = 2.2, h = bottom_h);
    slot_w = 1.4;
    hull() {
      translate([-(slot_w - bottom_dia_reduce) / 2, -10, -0.1]) cube([slot_w - bottom_dia_reduce, 20, 0.1]);
      translate([-slot_w / 2, -10, bottom_h - 0.1001]) cube([slot_w, 20, 0.1]);
    }
    translate([-5, (bottom_base_holes_inner_dia - reduce)/2 - 0.6, -0.001]) cube([10, 5, bottom_h]);
    translate([-5, -5-(bottom_base_holes_inner_dia - reduce)/2 + 0.6, -0.001]) cube([10, 5, bottom_h]);
    translate([-5, bottom_base_holes_outer_dia/2 - 1, 0]) cube([10, 5, 20]);

    translate([0, 0, bottom_h + bottom_base_holes_h - 0.2]) rotate([-90, 0, 0]) m2_nut_cutout(expand_h = 0.2);
  }
}

module xl_330_base_top_bracket() {
  h_reduce = 0.2;
  top_ring_h = bearing_top_gap;

  module anchor_block() {
    anchor_block_w = xl_330_w + 24;
    anchor_block_d = 20;
    anchor_block_h = 6;
    translate([0, 0, top_ring_h + 2]) {
      difference() {
        hull() {
          translate([-anchor_block_w / 2, -anchor_block_d / 2, 0]) {
            cube([anchor_block_w, anchor_block_d, 0.1]);
          }
          translate([-anchor_block_w / 2 + 5, -anchor_block_d / 2, anchor_block_h]) {
            cube([anchor_block_w - 10, anchor_block_d, 0.1]);
          }
        }
        translate([-anchor_block_w / 2 - 1+3, -4, 0]) cube([anchor_block_w + 2-6, 8, anchor_block_h + 1]);
        translate([-anchor_block_w / 2 + 7, anchor_block_d / 2 + 1, 3]) rotate([90, 0, 0]) cylinder(d = m2_screw_hole_d, h = anchor_block_d + 2);
        translate([anchor_block_w / 2 - 7, anchor_block_d / 2 + 1, 3]) rotate([90, 0, 0]) cylinder(d = m2_screw_hole_d, h = anchor_block_d + 2);
      }
    }
  }

  module base_bracket() {
    difference() {
      union() {
        cylinder(d = bearing_inner_od, h = top_ring_h + xl_330_bracket_thickness);
        difference() {
          intersection() {
            translate([0, 0, top_ring_h]) cylinder(d = xl_330_base_cover_w - 6, h = 2);
            top_ring_d = xl_330_bottom_bracket_d();
            translate([-xl_330_base_cover_w / 2, -top_ring_d / 2, top_ring_h]) cube([xl_330_base_cover_w, top_ring_d, 2]);
          }
          cutout_w = xl_330_bottom_bracket_w();
          cutout_d = xl_330_bottom_bracket_d();
          translate([-cutout_w / 2, -cutout_d / 2, top_ring_h]) cube([cutout_w, cutout_d, 20]);      
        }      
        translate([-xl_330_w / 2, -xl_330_d / 2, top_ring_h + xl_330_bracket_thickness]) xl_330_bottom_bracket();
        anchor_block();
      }
      // cut out floor of bracket a bit more (when printed upside down, the floor will be unevent)
      translate([-xl_330_w / 2 - xl_330_bracket_gap, -xl_330_d / 2 - xl_330_bracket_gap, top_ring_h + xl_330_bracket_thickness - 0.4]) xl_330_hull(extend = xl_330_bracket_gap * 2);
    }
  }      
      
  difference() {
    union() {
      translate([0, 0, h_reduce]) {
        cylinder(d = bearing_id, h = top_cover_plate_thickness + bearing_h - h_reduce);
        translate([0, 0, top_cover_plate_thickness + bearing_h - h_reduce]) base_bracket();
      }
    }
    translate([0, 0, -1]) {
      cylinder(d = xl_330_horn_dia + xl_330_joint_horn_dia_extend, h = xl_330_horn_h + 1);
      xl_330_horn_holes(h = 10, dia = 1.4);
    }
    translate([0, 0, xl_330_joint_d - h_reduce]) cylinder(d = xl_330_horn_dia, h = 10);
  }
}

xl_330_base_top_bracket_bottom_plate_h = m2_nut_h;
xl_330_base_top_bracket_top_ring_h = bearing_top_gap + xl_330_bracket_thickness + xl_330_base_top_bracket_bottom_plate_h;
  
cover_bearing_od_add = bottom_base_w - bearing_od;

base_bracket_gap = xl_330_bracket_gap + 0.1; // increase gap slightly as the base "box" surrounds the servo fully, and it would be too tight
xl_330_base_cover_w = bearing_od + cover_bearing_od_add;
xl_330_base_cover_d = (bearing_od + cover_bearing_od_add) / 2 + xl_330_horn_z_pos + (xl_330_bracket_thickness + base_bracket_gap) * 2;

function xl_330_base_cover_total_height() = bearing_h + top_cover_plate_thickness;

module _to_post_pos() {
  for (a = post_angles) {
    rotate(a) translate([0, -post_r]) {
      children();
    }
  }
}

module xl_330_base_cover() {
  w = xl_330_base_cover_w;
  d = xl_330_h - xl_330_horn_z_pos + xl_330_horn_dia / 2 + 2 + (xl_330_bracket_thickness + base_bracket_gap) * 2;
  h = xl_330_base_cover_total_height();
  d_full = xl_330_base_cover_d;

  difference() {
    cylinder(d = w, h = h);
    translate([0, 0, h - bearing_h]) cylinder(d = bearing_od, h = bearing_h + 1);
    translate([0, 0, -1]) {
      cylinder(d = bearing_od - 2, h = h);
    }
    _to_post_pos() {
      translate([0, 0, -1]) {
        cylinder(d = post_hole_d, h = h + 2);
        translate([0, 0, 2]) cylinder(d = 4, h = h + 2);
      }
    }
  }
}

post_w = m2_screw_hole_d + 3;
post_r = xl_330_base_cover_w / 2 - post_w/2 - 2;
post_angles = [-45, 45, 135, -135];
post_hole_d = 2.2;
  
bottom_thickness = 1.6;
bottom_raise = 6;
bottom_h = bottom_thickness + bottom_raise;

module xl_330_base_bottom() {

  w = xl_330_w + (xl_330_bracket_thickness + base_bracket_gap) * 2;
  d = xl_330_h + (xl_330_bracket_thickness + base_bracket_gap) * 2;
  h = bottom_raise + bottom_thickness + xl_330_d;

  // posts connected to the inner box
  module posts_to_box() {
    post_h = bottom_raise + bottom_thickness + xl_330_d;
    _to_post_pos() {
      difference() {
        cylinder(d = post_w, h = post_h);
        translate([0, 0, -1]) cylinder(d = post_hole_d, h = post_h + 2);
      }
    }    
    hull() {
      rotate(post_angles[0]) translate([0, -post_r + post_w / 2]) cylinder(d = xl_330_bracket_thickness, h = h);
      cylinder(d = xl_330_bracket_thickness, h = h);
    }
    hull() {
      rotate(post_angles[1]) translate([0, -post_r + post_w / 2]) cylinder(d = xl_330_bracket_thickness, h = h);
      cylinder(d = xl_330_bracket_thickness, h = h);
    }
    hull() {
      translate([-post_w / 2, 0, 0]) rotate(post_angles[2]) translate([0, -post_r]) cylinder(d = xl_330_bracket_thickness, h = h);
      translate([post_w / 2, 0, 0]) rotate(post_angles[3]) translate([0, -post_r]) cylinder(d = xl_330_bracket_thickness, h = h);
    }
  }

  inner_box_offset = [-w / 2, - xl_330_h + xl_330_horn_z_pos - xl_330_bracket_thickness - base_bracket_gap, 0];

  difference() {
    union() {
      translate(inner_box_offset) cube([w, d, h]);
      posts_to_box();
    }
    translate(inner_box_offset) {
      // box inside cutout
      translate([xl_330_bracket_thickness, xl_330_bracket_thickness, bottom_thickness]) cube([xl_330_w + base_bracket_gap * 2, xl_330_h + base_bracket_gap * 2, h + 1]);
      // circular cutouts from inner walls
      translate([w / 2, -1, w / 2 + 3]) rotate([-90, 0, 0]) cylinder(d = w - 2, h = d + 2);
      translate([w + 2 - 1, d / 2, w / 2 + 4]) rotate([0, -90, 0]) cylinder(d = d - 12, h = w + 2);
    }    
  }

  // platform to raise servo
  raise_w = xl_330_w - xl_330_conn_w * 2 - 3;
  translate(inner_box_offset) {
    translate([(w - raise_w) / 2, d/4, bottom_thickness]) {
      cube([raise_w, d/2, bottom_raise]);
    }
  }
  
  bottom_post_dia = bottom_base_holes_outer_dia + 2;

  // base plate ("cross")
  difference() {
    union() {
      for (pos = bottom_base_holes_pos) {
        hull() {
          cylinder(d = bottom_post_dia, h = bottom_thickness);
          translate(pos) {
            cylinder(d = bottom_post_dia, h = bottom_thickness);
          }
        }
      }
    }
    for (pos = bottom_base_holes_pos) {
      translate(pos - [0, 0, 1]) cylinder(d=3.4, h=20);
    }    
  }

}

function xl_330_base_bottom_total_height() = xl_330_d + bottom_h;

function xl_330_base_servo_z_offset() = bottom_h;

function xl_330_base_bracket_servo_z_offset() = top_cover_plate_thickness + xl_330_bracket_thickness + bearing_top_gap + bearing_h;


//----------------------------------------------------------------------------------
// Demo
//----------------------------------------------------------------------------------
module demo() {
  difference() {
    union() {
      %translate([-xl_330_w / 2, xl_330_horn_z_pos, -xl_330_d - xl_330_joint_bracket_y_gap]) rotate([90, 0, 0]) xl_330();
      xl_330_base_top_bracket();
      xl_330_base_cover();      
      %translate([-xl_330_w / 2, -xl_330_d / 2, xl_330_base_bracket_servo_z_offset()]) {
        xl_330();
      }
    }
    cube([40,40,40]);
  }
  
  translate([0, 0, -xl_330_d - bottom_h - 0.3]) {
    xl_330_base_bottom();
    translate([0, 0, -bottom_base_h-1]) {
      for (pos = bottom_base_holes_pos) {
        translate(pos) {
          translate([0, 0, -6 -3.4]) rotate(180) plug2();
        }
      }
    }
  }
}


demo();


//----------------------------------------------------------------------------------
// Print
//----------------------------------------------------------------------------------

//rotate([180,0,0]) xl_330_base_top_bracket();
//xl_330_base_cover();
//xl_330_base_bottom();
//plug2();
//plug2(reduce = 0.4);
