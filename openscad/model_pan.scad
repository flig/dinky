$fn = 50;

include <xl-330.scad>
use <xl-330-base.scad>

translate([0, 0, -xl_330_base_cover_total_height()]) {
  xl_330_base_top_bracket();

  translate([-xl_330_w / 2, -xl_330_d / 2, xl_330_base_bracket_servo_z_offset()]) {
    xl_330();
  }
}

total_height = xl_330_base_bracket_servo_z_offset() + xl_330_h - xl_330_base_cover_total_height();

pan_joint_z_offset =  xl_330_base_bracket_servo_z_offset() - xl_330_base_cover_total_height() + xl_330_horn_z_pos;

echo("pan_height", total_height / 1000);
echo("pan_joint_z_offset", pan_joint_z_offset / 1000);
