// Copyright 2022, Pal Denes. Licensed under the AGPL v3, see COPYING.
#include <memory>
#include <vector>
#include <algorithm>
#include <chrono>

#include "cv_bridge/cv_bridge.h"
#include "image_transport/image_transport.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "sensor_msgs/msg/camera_info.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include "sensor_msgs/point_cloud2_iterator.hpp"
#include "tf2/exceptions.h"
#include "tf2_ros/transform_listener.h"
#include "tf2_ros/buffer.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"

using std::placeholders::_1;
using namespace std::chrono_literals;

// Apple diameter (in mm)
const int APPLE_DIA = 40;
// Min. radius to consider (in pixels)
const int MIN_RADIUS_PX = 10;
// Size used for the kernel in cv::erode
const int EROSION_SIZE = 9;
// Default threshold value for red in the V channel
const int V_THRESHOLD = 150;

const double PUBLISH_RATE = 15.0;

const char* CAMERA_FRAME = "camera";
const char* BASE_FRAME = "base_link";

auto const EROSION_KERNEL =
  cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(EROSION_SIZE * 2 + 1, EROSION_SIZE * 2 + 1));
auto const BLUR_SIZE = cv::Size(9, 9);
auto const WHITE = cv::Scalar(255, 255, 255);

bool compare_z(const cv::Point3f & p1, const cv::Point3f & p2) {
  return p1.z < p2.z;
}

// TODO make this "smarter": only work when have subscribers (this applies to the image too: remove the debug param and rely on subscribers!)

class SeeApples : public rclcpp::Node {

public:
  SeeApples() : Node("see_apples") {
    rcl_interfaces::msg::ParameterDescriptor debug_param_desc;
    debug_param_desc.description = "publish image for debugging";
    this->declare_parameter<bool>("debug", false, debug_param_desc);

    rcl_interfaces::msg::ParameterDescriptor v_threshold_param_desc;
    v_threshold_param_desc.description = "threshold for red in V channel";
    this->declare_parameter<int>("v_threshold", V_THRESHOLD, v_threshold_param_desc);

    rcl_interfaces::msg::ParameterDescriptor publish_rate_param_desc;
    v_threshold_param_desc.description = "publish rate";
    this->declare_parameter<double>("publish_rate", PUBLISH_RATE, publish_rate_param_desc);

    // TODO is this correct / the simplest way?? Looks like we need to wrap the subscription in a shared ptr in order to be able to call reset on it
    // (to unsubscribe, see in the callback fn)
    cam_info_sub_ = std::make_shared<rclcpp::Subscription<sensor_msgs::msg::CameraInfo>::SharedPtr>(this->create_subscription<sensor_msgs::msg::CameraInfo>("camera_info", 10, std::bind(&SeeApples::camera_info_callback, this, _1)));

    tf_buffer_ = std::make_unique<tf2_ros::Buffer>(this->get_clock());
    transform_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

    publish_period_ = rclcpp::Duration::from_seconds(1.0 / this->get_parameter("publish_rate").get_value<double>());

    RCLCPP_INFO(this->get_logger(), "started");
  }

private:
  void publish_image(const cv::Mat & cv_img) {
    cv_bridge::CvImage cvi;
    cvi.encoding = sensor_msgs::image_encodings::MONO8;
    cvi.image = cv_img;
    auto v_msg = cvi.toImageMsg();
    v_msg->header.frame_id = CAMERA_FRAME;
    image_pub_.publish(v_msg);
  }

  // image from camera is yuv422, we extract V and scale it x2 horizontally
  cv::Mat yuv422_to_v(const sensor_msgs::msg::Image::ConstSharedPtr & msg) {
    auto image = cv_bridge::toCvShare(msg)->image;
    auto orig_size = cv::Size(image.cols, image.rows);
    cv::extractChannel(image, image, 1);
    image = image.reshape(2);
    cv::extractChannel(image, image, 1);
    cv::resize(image, image, orig_size);
    return image;
  }

  // image from camera is rgb8, we convert it to YUV and extract V
  cv::Mat rgb_to_v(const sensor_msgs::msg::Image::ConstSharedPtr & msg) {
    auto image = cv_bridge::toCvShare(msg)->image;
    cv::cvtColor(image, image, cv::COLOR_BGR2YUV);
    cv::extractChannel(image, image, 1);
    return image;
  }

  void draw_circle(const cv::Mat & img, const cv::Point2f & center, const float & radius) {
    cv::circle(img, center, cvRound(radius), WHITE, 1);
  }

  cv::Point3f get_pos(const int img_w, const int img_h, const cv::Point2f center, const float radius) {
    // from https://en.wikipedia.org/wiki/Pinhole_camera_model:
    // height of image (mm) = focal length (mm) * height of object (mm) / distance of object (mm)
    // -> z = distance of object (mm) = focal length (mm) * height of object (mm) / height of image (px)
    // -> x and y: just multiply by height of object (mm) / height of image (px) (and we also center these)
    return cv::Point3f {
      (center.x - static_cast<float>(img_w) / 2) * APPLE_DIA / (radius * 2),
      (center.y - static_cast<float>(img_h) / 2) * APPLE_DIA / (radius * 2),
      (APPLE_DIA * focal_len_) / (radius * 2)
    };
  }

  std::vector<cv::Point3f> process(const cv::Mat & img, int v_threshold) {
    cv::GaussianBlur(img, img, BLUR_SIZE, 0);
    cv::threshold(img, img, v_threshold, 255, cv::THRESH_BINARY);
    cv::erode(img, img, EROSION_KERNEL);

    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(img, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

    cv::Point2f center;
    float radius;
    std::vector<cv::Point> contours_poly;
    std::vector<cv::Point3f> positions;
    for (auto & contour : contours) {
      contours_poly.clear();
      cv::approxPolyDP(contour, contours_poly, 3, true);
      cv::minEnclosingCircle(contours_poly, center, radius);
      radius += EROSION_SIZE;
      if (radius > MIN_RADIUS_PX) {
        positions.push_back(get_pos(img.cols, img.rows, center, radius));
        if (debug_) {
          draw_circle(img, center, radius);
        }
      }
    }
    std::sort(positions.begin(), positions.end(), compare_z);
    return positions;
  }

  void publish_cloud(const std::vector<cv::Point3f> & positions, const geometry_msgs::msg::TransformStamped & transform_msg) {

    tf2::Transform transform;
    tf2::fromMsg(transform_msg.transform, transform);

    sensor_msgs::msg::PointCloud2 cloud_msg;
    cloud_msg.header.frame_id = "base_link";
    cloud_msg.header.stamp = transform_msg.header.stamp;
    sensor_msgs::PointCloud2Modifier modifier(cloud_msg);
    modifier.setPointCloud2FieldsByString(1, "xyz");
    modifier.resize(positions.size());
    sensor_msgs::PointCloud2Iterator<float> iter_x(cloud_msg, "x");
    sensor_msgs::PointCloud2Iterator<float> iter_y(cloud_msg, "y");
    sensor_msgs::PointCloud2Iterator<float> iter_z(cloud_msg, "z");

    for(size_t i = 0; i < positions.size(); ++i, ++iter_x, ++iter_y, ++iter_z) {
      auto p = tf2::Vector3 {
        positions[i].x / 1000.0,
        positions[i].y / 1000.0,
        positions[i].z / 1000.0
      };
      auto out = transform * p;
      *iter_x = out[0];
      *iter_y = out[1];
      *iter_z = out[2];
    }

    cloud_pub_->publish(cloud_msg);
  }

  void image_callback(const sensor_msgs::msg::Image::ConstSharedPtr & msg) {
    auto now = this->now();
    if (now < (last_publish_time_ + publish_period_)) {
      return;
    }
    last_publish_time_ = now;

    this->get_parameter("debug", debug_);

    if (debug_) {
      RCLCPP_INFO(this->get_logger(), "image_callback: %dx%d %s", msg->width, msg->height, msg->encoding.c_str());
    }

    auto time_start = std::chrono::steady_clock::now();

    cv::Mat img;
    if (msg->encoding == "yuv422") {
      img = yuv422_to_v(msg);
    } else if (msg->encoding == "rgb8") {
      img = rgb_to_v(msg);
    } else {
      RCLCPP_ERROR(this->get_logger(), "unknown encoding: %s", msg->encoding.c_str());
      return;
    }

    int v_threshold;
    this->get_parameter("v_threshold", v_threshold);

    try {
      auto positions = process(img, v_threshold);
      if (positions.size() > 0) {
        geometry_msgs::msg::TransformStamped transform_msg =
          tf_buffer_->lookupTransform(BASE_FRAME, CAMERA_FRAME, this->get_clock()->now(), 500ms);
        publish_cloud(positions, transform_msg);
      }
    } catch (tf2::TransformException & ex) {
      RCLCPP_INFO(this->get_logger(), "Could not transform %s to %s: %s",
                  CAMERA_FRAME, BASE_FRAME, ex.what());
      return;
    }

    if (debug_) {
      auto time_end = std::chrono::steady_clock::now();
      elapsed_times_total_ +=
        std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start).count();
      ++elapsed_times_count_;
      RCLCPP_INFO(this->get_logger(), "elapsed: %ldus  avg: %ldus  cnt: %ld",
                  std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start).count(),
                  elapsed_times_total_ / elapsed_times_count_, elapsed_times_count_);
      publish_image(img);
    }
  }

  void camera_info_callback(const sensor_msgs::msg::CameraInfo::SharedPtr msg) {
    RCLCPP_INFO(this->get_logger(), "got camera_info:  fx: %f", msg->k[0]);
    focal_len_ = msg->k[0];
    // https://answers.ros.org/question/354792/rclcpp-how-to-unsubscribe-from-a-topic/
    cam_info_sub_->reset();

    // We can properly start now:
    image_transport::ImageTransport it(shared_from_this());
    image_sub_ = it.subscribe("image_raw", 1, std::bind(&SeeApples::image_callback, this, _1));
    image_pub_ = it.advertise("apples/image", 1);
    cloud_pub_ = this->create_publisher<sensor_msgs::msg::PointCloud2>("apples", 10);
    last_publish_time_ = this->now();

    RCLCPP_INFO(this->get_logger(), "started pub/sub, will publish at %.2f Hz", 1.0 / publish_period_.seconds());
  }

  std::shared_ptr<rclcpp::Subscription<sensor_msgs::msg::CameraInfo>::SharedPtr> cam_info_sub_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  std::shared_ptr<tf2_ros::TransformListener> transform_listener_{nullptr};
  std::unique_ptr<tf2_ros::Buffer> tf_buffer_;
  rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr cloud_pub_;
  int focal_len_;
  bool debug_;
  rclcpp::Duration publish_period_{0ms};
  rclcpp::Time last_publish_time_;

  int64_t elapsed_times_total_ = 0;
  int64_t elapsed_times_count_ = 0;
};

int main(int argc, char * argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<SeeApples>());
  rclcpp::shutdown();
  return 0;
}
