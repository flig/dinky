# Setting up a Pi for Dinky

## Hardware

- Raspberry Pi Zero 2 W

- Camera

- Connections: **TODO**


## OS image and basic settings

We used a beta (lite) version of the 64-bit Pi OS (the latest one available at the time).
See the details of preparing the image here: https://bldrbts.me.uk/posts/2021/11/28/ros-2-on-a-raspberry-pi-zero-2-w/

Important config file settings:

In `/boot/config.txt`:

    # shutdown button
    dtoverlay=gpio-shutdown,gpio_pin=3

    # enable the camera
    start_x=1
    gpu_mem=128
    
    # use UART0 as primary
    dtoverlay=disable-bt
    enable_uart=1

    # USB Ethernet Gadget
    dtoverlay=dwc2

In `/boot/cmdline.txt`: append to the end:

    ...rootwait modules-load=dwc2,g-ether

In `/etc/hostname`: 

    dinkyarm

(With the above, it should be possible to connect the Pi via USB to any machine with Zeroconf/Bonjour support and `ssh pi@dinkyarm.local`.)


## ROS

Install ROS 2 Galactic (compile it locally).
Again, see the details here: https://bldrbts.me.uk/posts/2021/11/28/ros-2-on-a-raspberry-pi-zero-2-w/

All ROS packages are installed/compiled manually in `/home/pi`: the base packages are in `/home/pi/ros2_galactic`.


## OpenCV

The version of OpenCV that comes with the OS is too old (3.x).
A more recent version, 4.5.4 is included in the new Pi 2 W Buster image released by Q-engineering:
https://github.com/Qengineering/RPi_64-bit_Zero-2-image -- this is where OpenCV was extracted from. (We could have tried using this image in the first place, but by this time the OS and ROS were already set up and working, so it seemed simpler and safer to keep the working image.)

1. Download the image from https://drive.google.com/uc?id=1wJeuIGhs-49lcPQDJoM-AF-IdkAODVYe&export=download (`RPi_64OS_Zero_2.img`).

1. Copy the following files from it:
   
        /usr/local/bin/opencv_*
        /usr/local/bin/setup_vars_opencv4.sh 
        /usr/local/include/opencv4/
        /usr/local/lib/libopencv_*
        /usr/local/lib/cmake/opencv4/
        /usr/local/lib/pkgconfig/
        /usr/local/lib/python3.7/dist-packages/cv2/
        /usr/local/lib/libtbb.so
        /usr/local/share/opencv4/


## `v4l2_camera`

This is a ROS 2 camera driver using Video4Linux2: https://gitlab.com/boldhearts/ros2_v4l2_camera

Based on the [installation instructions](https://medium.com/swlh/raspberry-pi-ros-2-camera-eef8f8b94304):

1. First, get the sources:

        mkdir -p ros2_v4l2_camera/src
        cd ros2_v4l2_camera/src
        git clone --branch foxy https://gitlab.com/boldhearts/ros2_v4l2_camera.git
        git clone --branch ros2 https://github.com/ros-perception/vision_opencv.git
        git clone --branch foxy-devel https://github.com/ros-perception/image_transport_plugins.git
        cd ..

(Note that for `image_transport_plugins` the correct branch is `foxy-devel`, not `ros2`. This has probably changed since the above post was written. Also, we're not installing `image_common` here, as it's already included in Galactic, installed in `~/ros2_galactic`.)

1. Build it (doesn't take too long):

        . ~/ros2_galactic/install/local_setup.bash
        rosdep install --from-paths src -r -y
        export MAKEFLAGS=-j1
        colcon build --symlink-install --executor sequential --cmake-args -DOpenCV_DIR=/usr/local/lib/cmake/opencv4

To test it:

- Start the node on the Pi:

        ros2 run v4l2_camera v4l2_camera_node -p image_size:=[320,240]

  (The default image size is 640x480.)

- Show image with RViz (on a desktop):

    - At the top, in Global Options, set Fixed frame to `camera`
    - Add an Image panel with the topic `/image_raw`


## Camera calibration 

This step can be used to generate a "rectified" image, removing distortions -- but most importantly for us, it also determines the focal length of the camera, which we'll need later. (Tutorials: https://navigation.ros.org/tutorials/docs/camera_calibration.html and https://jeffzzq.medium.com/ros2-image-pipeline-tutorial-3b18903e7329)

This can be run on another machine with desktop (with ROS2 Galactic installed):

1. Install the ROS package and OpenCV for Python:

        apt-get install ros-galactic-camera-calibration
        pip install cv2

1. Print out a checkerboard pattern. (E.g. using https://calib.io/pages/camera-calibration-pattern-generator as in the first tutorial: board w/h: 200x150, rows/cols: 8x10, checker width: 15.)

1. Start `v4l2_camera` on the Pi (see above).

1. Run the calibration program on the desktop and move around the pattern until calibration is complete:

        ros2 run camera_calibration cameracalibrator --size 7x9 --square 0.20 image:=/image_raw camera:=/my_camera

1. The results are saved to `/tmp/calibrationdata.tar.gz`. Unpacking it, we get a `ost.yaml` file. 

1. Edit it and change `camera_name` to `mmal_service_16.1` so that it matches the device seen by `v4l2_camera` (it's probably possible to set this initially too, when running the calibration).

1. Copy that to the Pi, into the default location where `v4l2_camera` is looking for it: `/home/pi/.ros/camera_info/mmal_service_16.1.yaml`

